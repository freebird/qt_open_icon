﻿#ifndef JoyStick_H
#define JoyStick_H

#include "platformhead.h"

class JoyStick : public QWidget
{
    Q_OBJECT

public:
    explicit JoyStick(QWidget *parent = 0);
    ~JoyStick();

private:

    void initGui();
    void initData();
    void sendCommand(const char *command);

    // 小车控制命令
    const char *forward;
    const char *backward;
    const char *turnLeft;
    const char *turnRight;
    const char *stopRun;

    const char *rightFront;
    const char *leftFront;
    const char *leftRear;
    const char *rightRear;

    const char *accelerated;
    const char *slowdown;

    bool mousePressed;
    bool mouseControl;

    int x;
    int y;

    int r1;
    int r2;
    int r3;
    int h;  //  等腰三角形高度

    qreal angle;
    //  记录前一个按键状态
    int keyDown;
    int direction;
//qreal wid;
private slots:
    void recvSlot(QString str);

protected:
    virtual void keyPressEvent(QKeyEvent *ev);
    virtual void keyReleaseEvent(QKeyEvent *ev);
    virtual void paintEvent(QPaintEvent *e);

    virtual void mousePressEvent(QMouseEvent *e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void mouseReleaseEvent(QMouseEvent *e);

    virtual void resizeEvent(QResizeEvent *e);

//    virtual bool event(QEvent *e);

signals:
    void stateSig(int);
};

#endif // JoyStick_H
