/**
 ** @author:	 Greedysky
 ** @date:       2019.4.19
 ** @brief:      进度条组件
 */
#ifndef LOADINGPROGRESSBAR_H
#define LOADINGPROGRESSBAR_H

#include <QTimer>
#include <QWidget>

struct Position
{
    double x;
    double y;
};

class LoadingProgressBar : public QWidget
{
    Q_OBJECT
public:
    explicit LoadingProgressBar(QWidget *parent = nullptr);

    void setDotCount(int count);

    void setDotColor(const QColor &color);

    void setMaxDiameter(float max);
    void setMinDiameter(float min);

    void start();

private:
    int m_index, m_count;
    QColor m_dotColor;
    float m_minDiameter, m_maxDiameter;

    int m_interval;
    QTimer m_timer;

    QList<float> m_ranges;
    QList<Position> m_dots;

protected:
    void paintDot(QPainter &painter);
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void resizeEvent(QResizeEvent *event) override;

};

#endif
