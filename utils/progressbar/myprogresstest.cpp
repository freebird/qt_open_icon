#include "myprogresstest.h"
#include "ui_myprogresstest.h"

MyProgressTest::MyProgressTest(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::MyProgressTest),
      m_value(0)
{
    ui->setupUi(this);

    ui->widget_1->setSize(64);
    ui->widget_1->setColor(QColor(255, 0, 0));

    ui->widget_2->setLineWidth(8);
    ui->widget_2->setSize(74);
    ui->widget_2->setColor(QColor(0, 255, 0));

    ui->widget_3->setLineWidth(13);
    ui->widget_3->setSize(84);
    ui->widget_3->setColor(QColor(0, 0, 255));

    ui->widget_4->setFixedSize(200, 200);
    ui->widget_4->start();

    ui->widget_5->setFixedSize(200, 200);

    ui->widget_6->setOutterBarWidth(20);
    ui->widget_6->setInnerBarWidth(20);
    ui->widget_6->setControlFlags(RoundProgressBar::All);

    ui->widget_7->setInnerDefaultTextStyle(RoundProgressBar::ValueAndMax);
    ui->widget_7->setOutterBarWidth(12);
    ui->widget_7->setInnerBarWidth(20);
    ui->widget_7->setOutterColor(QColor(150, 50, 240));
    ui->widget_7->setDefaultTextColor(QColor(255, 190, 57));
    ui->widget_7->setInnerColor(QColor(255, 190, 57), QColor(255, 230, 129));
    ui->widget_7->setControlFlags(RoundProgressBar::All);

    ui->widget_8->setInnerDefaultTextStyle(RoundProgressBar::Value);
    ui->widget_8->setOutterBarWidth(4);
    ui->widget_8->setInnerBarWidth(20);
    ui->widget_8->setOutterColor(QColor(250, 55, 56));
    ui->widget_8->setDefaultTextColor(QColor(255, 19, 157));
    ui->widget_8->setInnerColor(QColor(255, 19, 157), QColor(255, 230, 129));
    ui->widget_8->setControlFlags(RoundProgressBar::All);


    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), SLOT(updateRender()));

    m_timer->start(800);
}

MyProgressTest::~MyProgressTest()
{
    delete ui;
}

void MyProgressTest::updateRender()
{
    if(m_value >= 100)
    {
        m_value = 0;
    }
    else
    {
        m_value += 5;
    }

    ui->widget_5->setValue(m_value);
    ui->widget_6->setText(qrand() % 100 + 1);
    ui->widget_7->setText(qrand() % 100 + 1);
    ui->widget_8->setText(qrand() % 100 + 1);
}
