/*
* Copyright (C) 2019 ~ 2021 Uniontech Software Technology Co.,Ltd.
*
* Author:     wangyou <wangyou@uniontech.com>
*
* Maintainer: wangyou <wangyou@uniontech.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "lineedittipwidget.h"

#include <QPainter>
#include <QHBoxLayout>
#include <QLabel>
#include <QDebug>

LineEditTipWidget::LineEditTipWidget(QWidget* parent)
    :QLineEdit(parent)
{
    setAutoFillBackground(true);

    setStyleSheet("background-color:rgba(255,255,255,255)");

    tipText = new QLabel(this);
    tipText->setStyleSheet("QLabel{background-color: transparent;}");

    hlayout = new QHBoxLayout(this);
    hlayout->setMargin(0);
    hlayout->setSpacing(0);
    hlayout->setContentsMargins(10, 0, 0, 0);
    hlayout->addWidget(tipText);
    setLayout(hlayout);
    m_font = tipText->font();
    m_font.setPixelSize(height()-10);
    tipText->setFont(m_font);
    tipText->setFixedHeight(m_font.pixelSize());

    qWarning()<<this->height();

}


LineEditTipWidget::~LineEditTipWidget()
{

}


void LineEditTipWidget::soltFocusChange(bool isHasFocus)
{
    qWarning()<<isHasFocus;
}

void LineEditTipWidget::setPlaceholderText(const QString & placeholderText)
{
   tipText->setText(placeholderText);
   update();
}


void LineEditTipWidget::paintEvent(QPaintEvent * event)
{
    QLineEdit::paintEvent(event);

}


void LineEditTipWidget::focusOutEvent(QFocusEvent *event)
{
    QLineEdit::focusOutEvent(event);
    if(this->text().isEmpty()){
        tipText->setFont(m_font);
        hlayout->setContentsMargins(10, 0, 0, 0);
        tipText->setFixedHeight(m_font.pixelSize());
        qWarning()<<this->height();
    }

}

void LineEditTipWidget::focusInEvent(QFocusEvent *event)
{
    QLineEdit::focusInEvent(event);
    QFont font = tipText->font();
    font.setPixelSize(12);
    tipText->setFixedHeight(12);
    tipText->setFont(font);
    hlayout->setContentsMargins(10, 0, 0, height()-12-10);
    setTextMargins(0, 10, 0, 0);
    qWarning()<<this->height();
}
